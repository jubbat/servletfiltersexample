package com.journaldev.servlet.filters;

import java.io.IOException;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * This filter logs the parameters and cookies with each request.
 */
@WebFilter("/RequestLoggingFilter")
public class RequestLoggingFilter extends MyFilter {

  @Override
  public void init(FilterConfig config) {
    this.context = config.getServletContext();
    log("RequestLoggingFilter initialized");
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    logParameters(request);
    logCookies((HttpServletRequest) request);
    chain.doFilter(request, response);
  }

  private void logParameters(ServletRequest req) {
    Map<String, String[]> parameterMap = req.getParameterMap();
    for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
      String parameterName = entry.getKey();
      String parameterValue = entry.getValue()[0]; // assume there is only one value for each parameter
      String remoteAddress = req.getRemoteAddr();
      log(remoteAddress + "::Request Params::{" + parameterName + "=" + parameterValue + "}");
    }
  }

  private void logCookies(HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null)
      for (Cookie cookie : cookies) {
        String address = request.getRemoteAddr();
        String name = cookie.getName();
        String value = cookie.getValue();
        log(address + "::Cookie::{" + name + "=" + value + "}");
      }
  }

}
