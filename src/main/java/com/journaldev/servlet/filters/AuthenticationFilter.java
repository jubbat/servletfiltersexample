package com.journaldev.servlet.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter extends MyFilter {

  @Override
  public void init(FilterConfig config) {
    this.context = config.getServletContext();
    log("AuthenticationFilter initialized");
  }
  
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    String uri = httpRequest.getRequestURI();
    log("Requested Resource::" + uri);
    HttpSession session = httpRequest.getSession(false);
    
    boolean isNotALoginURL = !(uri.endsWith("html") || uri.endsWith("LoginServlet"));
    boolean sessionIsNotCreated = session == null;
    if(sessionIsNotCreated && isNotALoginURL) {
      log("Unauthorized access request"); 
      ((HttpServletResponse) response).sendRedirect("login.html");
    } else {
      chain.doFilter(request, response);
    }
  }
  
}
