package com.journaldev.servlet.filters;

import javax.servlet.Filter;
import javax.servlet.ServletContext;

abstract class MyFilter implements Filter {

  protected ServletContext context;

  void log(String msg) {
    if (context != null)
      this.context.log(msg);
  }

  @Override
  public void destroy() {
    //close any resources here
  }
}
