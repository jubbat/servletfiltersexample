package com.journaldev.servlet.session;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private final String userID = "admin";
  private final String password = "password";

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    String user = request.getParameter("user");
    String password = request.getParameter("password");
    boolean isValidCredentials = userID.equals(user) && this.password.equals(password);
    if (isValidCredentials) {
      HttpSession session = request.getSession();
      session.setAttribute("user", this.userID);
      session.setMaxInactiveInterval(30 * 60);
      Cookie usernameCookie = new Cookie("user", user);
      usernameCookie.setMaxAge(30 * 60);
      response.addCookie(usernameCookie);
      response.sendRedirect("LoginSuccess.jsp");
    } else {
      RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.html");
      PrintWriter responseWriter = response.getWriter();
      responseWriter.println("<font color=red>User name or password is wrong.</font>");
      rd.include(request, response);
    }
  }
}
