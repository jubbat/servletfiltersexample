package com.journaldev.servlet.session;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/LogoutServlet")
public class LogoutServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
    response.setContentType("text/html");
    Cookie[] cookies = request.getCookies();
    printJSESSIONID(cookies);
    HttpSession session = request.getSession(false);
    if(session != null) {
      System.out.println("User: " + session.getAttribute("user"));
      session.invalidate();
    }
    response.sendRedirect("login.html");
  }

  private void printJSESSIONID(Cookie[] cookies) {
    if(cookies != null)
      for(Cookie cookie : cookies)
        if(cookie.getName().equals("JSESSIONID")) {
          System.out.println("JESSIONID = " + cookie.getValue());
          return;
        }
  }
}
