<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login Success Page</title>
</head>
<body>
	<%
	  //Allow access only if session exists
	  String user = (String) session.getAttribute("user");
	  String userName = null;
	  String sessionID = null;
	  Cookie[] cookies = request.getCookies();
	  if (cookies != null)
	    for (Cookie cookie : cookies)
	      if (cookie.getName().equals("user"))
	        userName = cookie.getValue();
	      else if (cookie.getName().equals("JSESSIONID"))
	        sessionID = cookie.getValue();
	%>
	<h3>Hi, <%=userName%>. Login successful. Your session ID is <%=sessionID%></h3>
	<br> User: <%=user%>
	<br>
	<a href="CheckoutPage.jsp">Checkout Page</a>
	<form action="LogoutServlet" method="post">
		<input type="submit" value="Logout">
	</form>
</body>
</html>